ARG BASE_PYTHON_VERSION=3.13-slim-bookworm
FROM library/python:${BASE_PYTHON_VERSION}
ARG ANSIBLE_VERSION

ENV ANSIBLE_VERSION=${ANSIBLE_VERSION}
RUN if [ -z "${ANSIBLE_VERSION}" ]; then echo "required build arg ANSIBLE_VERSION is absent"; exit 1; fi && \
    apt-get update > /dev/null 2>&1                                && \
    apt-get install gcc libffi-dev ssh sshpass -y > /dev/null 2>&1 && \
    pip3 install --timeout 300 ansible==${ANSIBLE_VERSION} passlib ansible-lint > /dev/null 2>&1      && \
    apt-get remove gcc libffi-dev -y > /dev/null 2>&1              && \
    apt-get clean > /dev/null 2>&1                                 && \
    apt-get autoremove -y > /dev/null 2>&1                         && \
    rm -rf /var/lib/apt/lists/* > /dev/null 2>&1                   && \
    pip cache purge > /dev/null 2>&1

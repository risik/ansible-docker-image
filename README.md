# ansible-docker-image

Ansible Docker Image useful for different works with ansible

## Motivation

There are a lot of tasks I need to perform using ansible.
Both git-ops and ci-ops approaches require ansible.
Docker container is often required to run these tasks in CI as image.
But by some reason on docker hub there are no ready-to-use containers with ansible installed.
Installing ansible sometimes require additional 1-2 minutes.
These images should fix this gap.

## Bases

I use debian slim python image as base.

## Tags

Images have tag with 3 variables:

* ansible version (`ANSIBLE_VERSION`)
    * `9.0.1`
    * `9.1.0`
    * `9.2.0`
    * `9.3.0`
    * `9.4.0`
    * `9.5.1`
    * `9.6.1`
    * `9.7.0`
    * `9.8.0`
    * `9.9.0`
    * `9.10.0`
    * `9.11.0`
    * `9.12.0`
    * `10.0.1`
    * `10.1.0`
    * `10.2.0`
    * `10.3.0`
    * `10.4.0`      
    * `10.5.0`
    * `10.6.0`
    * `11.0.0`
    * `11.1.0`
    * `11.2.0`
* base python images versions (`BASE_PYTHON_VERSION`):
    * `3.12-slim-bookworm`
    * `3.13-slim-bookworm`

Full tag name: `${ANSIBLE_VERSION}-py-${BASE_PYTHON_VERSION}`
for example: `11.1.0-py-3.12-slim-bookworm`.

Additional tags:

* `9.0.1-py-3.12-slim-bookworm` -> `9.0`
* `9.1.0-py-3.12-slim-bookworm` -> `9.1`
* `9.2.0-py-3.12-slim-bookworm` -> `9.2`
* `9.3.0-py-3.12-slim-bookworm` -> `9.3`
* `9.4.0-py-3.12-slim-bookworm` -> `9.4`
* `9.5.1-py-3.12-slim-bookworm` -> `9.5`
* `9.6.1-py-3.12-slim-bookworm` -> `9.6`
* `9.7.0-py-3.12-slim-bookworm` -> `9.7`
* `9.8.0-py-3.12-slim-bookworm` -> `9.8`
* `9.9.0-py-3.12-slim-bookworm` -> `9.9`
* `9.10.0-py-3.12-slim-bookworm` -> `9.10`
* `9.11.0-py-3.12-slim-bookworm` -> `9.11`
* `9.12.0-py-3.12-slim-bookworm` -> `9.12` -> `9`
* `10.0.1-py-3.12-slim-bookworm` -> `10.0`
* `10.1.0-py-3.12-slim-bookworm` -> `10.1`
* `10.2.0-py-3.12-slim-bookworm` -> `10.2`
* `10.3.0-py-3.12-slim-bookworm` -> `10.3`
* `10.4.0-py-3.12-slim-bookworm` -> `10.4`
* `10.5.0-py-3.12-slim-bookworm` -> `10.5`
* `10.6.0-py-3.12-slim-bookworm` -> `10.6`
* `10.7.0-py-3.12-slim-bookworm` -> `10.7` -> `10`
* `11.0.0-py-3.12-slim-bookworm` -> `11.0`
* `11.1.0-py-3.12-slim-bookworm` -> `11.1`
* `11.2.0-py-3.12-slim-bookworm` -> `11.2` -> `11` -> `latest`

If you need another ones - MRs are welcome.
